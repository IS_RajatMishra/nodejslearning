const express = require('express')
const mongoose = require('mongoose')
const url = 'mongodb://localhost/AlienDBex' //connection string for create DB name with AlienDBex.

const app = express()

mongoose.connect(url, {useNewUrlParser:true})
const con = mongoose.connection

con.on('open', () => { // it will fire an event for open the connection
    console.log('connected...')
})

app.use(express.json())

const alienRouter = require('./routes/aliens')
app.use('/aliens',alienRouter)

app.listen(9000, () => {
    console.log('Server started')
})